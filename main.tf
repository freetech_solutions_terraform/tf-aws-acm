locals {
  final_domain = "${var.wildcard_enable == true ? "*.${var.domain_name}" : var.domain_name}"
}


resource "aws_acm_certificate" "this" {
  domain_name       = local.final_domain
  validation_method = var.validation_method

  tags = var.tags

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "this" {
  depends_on = ["aws_acm_certificate.this"]
  zone_id    = var.hosted_zone_id
  name       = aws_acm_certificate.this.domain_validation_options.0.resource_record_name
  type       = var.route53_record_type
  ttl        = "300"
  records    = [aws_acm_certificate.this.domain_validation_options.0.resource_record_value]
}