# tf-aws-acm
Use this resources to create a certificate using AWS Certification Manager (ACM). You should use you domain and Once created you should find your domain convertered in a wildcard domain,after that Route 53 will create a new CNAME dns record to validate it.


#IMPORTANT: 
**This repo doesn’t support the Email or None validation but you can use the DNS validation is a fast way to achieve it.**

## Example
```
module "example_acm_certificate" {
  source            = "../../modules
  domain_name = "hlesta.edranslab.com"
  hosted_zone_id = "Z1NTKV959MJPQ4"
  tags = module.tags.tags
  wildcard_enable = false
}

```
## Inputs

| Name | Description | Default | Required |
|------|-------------|:-----:|:-----:|
| domain_name | The name of the domain to which apply the cert | - | yes |
| validation_method | The method for validating the acm | `string` | yes |
| tags | A amount of tags added as a map | `<map>` | no |
| hosted_zone_id | The hosted zone id that you need to create new DNS records | - | no |
| wildcard_enable | Enable wildcard if you want, default is 0, you shouldn't add a wildcard if you don't change this variable as 1 | - | no |

## Outputs

| Name | Description |
|------|-------------|
| cert_arn | The ARN of the ACM. Will be of format `	arn:aws:acm:us-east-1:318949518667:certificate/1b1bbdae-13b3-40f0-9ed5-baaebdb0c0d3` |
| cert_domain_name | The domain name for which the certificate is issued |
| domain_validation_options | A list of data validation returned as array |
