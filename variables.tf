variable "domain_name" {
  description = "The name of the domain to which apply the cert"
}

variable "validation_method" {
  description = "Which method to use for validation. DNS or EMAIL are valid, NONE can be used for certificates that were imported into ACM and then into Terraform."
  default     = "DNS"
}

variable "tags" {
  description = "Our tags that includes edrans naming conventions"
  type        = "map"
  default     = {}
}

variable "hosted_zone_id" {
  description = "Hosted zone where we need to work"
  type        = "string"
}

variable "route53_record_type" {
  description = "Type of route53 records that you need to create"
  default     = "CNAME"
}

variable "wildcard_enable" {
  description = "Variable that allow us to choose the possibility of not use wildcard certificate"
  default     = false
}
