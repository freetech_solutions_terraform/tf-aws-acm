// The ARN of the bucket. Will be of format `arn:aws:s3:::bucketname`
output "cert_arn" {
  value       = aws_acm_certificate.this.arn
  description = "Set to the ARN of the found certificate"
}

output "cert_domain_name" {
  value       = aws_acm_certificate.this.domain_name
  description = "The domain name for which the certificate is issued"
}

output "domain_validation_options" {
  value       = aws_acm_certificate.this.domain_validation_options
  description = "A list of data validation returned as array"
}
